set :stage, :staging
set :branch, :master

# Simple Role Syntax
# ==================
#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

# Extended Server Syntax
# ======================
server '92.53.96.249', user: 'godlike2046', roles: %w{web app db}

# Folbert-comment: this should be set to the target directory of the deploy on the server.
# So if your site is placed in /home/few/sites/bedrock-test.com/, that is the path to use.
# Make sure the path starts at the root directory and doesn't end with a /
set :deploy_to, -> { "/home/g/godlike2046/pf/public_html" }

# Folbert-addition. We must change tmp dir since Oderland does not allow us to execute files placed in /tmp/
# Set it to a nice place, preferrably outside any public folders. Should not end with a /
set :tmp_dir, "/home/g/godlike2046/pf/tmp"

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
set :ssh_options, {
  keys: %w(~/.ssh/id_rsa),
  user: fetch(:user),
  auth_methods: %w(publickey)
  # auth_methods: %w(password)
}

#  set :ssh_options, {
#   keys: %w(~/.ssh/authorized_keys),
#   user: fetch(:user),
#   # auth_methods: %w(publickey)
#  #  forward_agent: false,
#  #  auth_methods: %w(password)
# }
fetch(:default_env).merge!(wp_env: :staging)
