// Slick
/* eslint-disable */
import Slick from 'slick-carousel/slick/slick';
/* eslint-enable */

// Glide
import Glide from '@glidejs/glide';

export default {
  init() {
    /* Knitwear product slider */
    var glideVertical = $('.pf-glide--vertical');
    if ($('.pf-glide--vertical').length) {
      const glideVerticalSliderCustomLength = function(Glide, Components, Events) {
        return {
          mount () {
            Events.emit('slider.length', Components.Sizes.length);
            $glideVerticalSlideTotal.text(Components.Sizes.length);
          },
        }
      }
      var $glideVerticalSlideCurrent = glideVertical.find('.pf-glide__current');
      var $glideVerticalSlideTotal = glideVertical.find('.pf-glide__total');
      var glideVerticalSlider = new Glide('.pf-glide--vertical', {
        type: 'carousel',
        perView: 1,
        gap: 0,
      });

      glideVerticalSlider.on(['mount.before', 'run.after'], function () {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (glideVerticalSlider.index ? glideVerticalSlider.index : 0) + 1;
        $glideVerticalSlideCurrent.text(i);
      });
      glideVerticalSlider.mount({glideVerticalSliderCustomLength});
    }

    /* Workshop product slider */
    if ($('.pf-workshop-product-slider').length) {
      const workshopProductSliderCustomLength = function(Glide, Components, Events) {
        return {
          mount () {
            Events.emit('slider.length', Components.Sizes.length);
            $workshopProductSlideTotal.text(Components.Sizes.length);
          },
        }
      }
      var $workshopProductSlideCurrent = $('.pf-workshop-product-slider .pf-glide__current');
      var $workshopProductSlideTotal = $('.pf-workshop-product-slider .pf-glide__total');
      var workshopProductSlider = new Glide('.pf-workshop-product-slider', {
        type: 'carousel',
        perView: 1,
        gap: 0,
      });

      workshopProductSlider.on(['mount.before', 'run.after'], function () {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (workshopProductSlider.index ? workshopProductSlider.index : 0) + 1;
        $workshopProductSlideCurrent.text(i);
      });
      workshopProductSlider.mount({workshopProductSliderCustomLength});
    }

    /* Hero slider */
    if ($('.pf-hero-slider').length) {
      const heroSliderCustomLength = function(Glide, Components, Events) {
        return {
          mount () {
            Events.emit('slider.length', Components.Sizes.length);
            $heroSlideTotal.text(Components.Sizes.length);
          },
        }
      }
      var $heroSlideCurrent = $('.pf-hero-slider .pf-glide__current');
      var $heroSlideTotal = $('.pf-hero-slider .pf-glide__total');
      var heroSlider = new Glide('.pf-hero-slider', {
        type: 'carousel',
        autoplay: 6000,
        perView: 1,
        gap: 0,
      });

      heroSlider.on(['mount.before', 'run.after'], function () {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (heroSlider.index ? heroSlider.index : 0) + 1;
        $heroSlideCurrent.text(i);
      });
      heroSlider.mount({heroSliderCustomLength});
    }

    /* Vertical image slider */
    if ($('.pf-vertical-image-slider').length) {
      const verticalSliderCustomLength = function(Glide, Components, Events) {
        return {
          mount () {
            Events.emit('slider.length', Components.Sizes.length);
            $verticalSlideTotal.text(Components.Sizes.length);
          },
        }
      }
      var $verticalSlideCurrent = $('.pf-vertical-image-slider .pf-glide__current');
      var $verticalSlideTotal = $('.pf-vertical-image-slider .pf-glide__total');
      var verticalSlider = new Glide('.pf-vertical-image-slider', {
        type: 'carousel',
        perView: 1,
        gap: 0,
      });

      verticalSlider.on(['mount.before', 'run.after'], function () {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (verticalSlider.index ? verticalSlider.index : 0) + 1;
        $verticalSlideCurrent.text(i);
      });
      verticalSlider.mount({verticalSliderCustomLength});
    }

    /* Knitwear slider */
    var $knitwearSlickSlider = $('.pf-knitwear-slider');
    var $knitwearSlickElement = $knitwearSlickSlider.children('.pf-slick');
    var $knitwearSlideCounter = $knitwearSlickSlider.children('.pf-slick-counter');
    var $knitwearSlideCurrent = $knitwearSlideCounter.children('.pf-slick-counter__current');
    var $knitwearSlideTotal = $knitwearSlideCounter.children('.pf-slick-counter__total');

    $knitwearSlickElement.on('init reInit afterChange', function (event, slick, currentSlide) {
      //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
      var i = (currentSlide ? currentSlide : 0) + 1;
      $knitwearSlideCurrent.text(i);
      $knitwearSlideTotal.text(slick.slideCount);

      if(slick.slideCount <= 1) {
        $knitwearSlideCounter.addClass('hidden');
      } else {
        $knitwearSlideCounter.removeClass('hidden');
      }
    });

    function knitwearSlickify(){
      $knitwearSlickElement.not('.slick-initialized').slick({
        draggable: false,
        infinite: true,
        pauseOnHover: false,
        swipe: false,
        touchMove: false,
        rows: 2,
        slidesPerRow: 4,
        adaptiveHeight: true,
        speed: 1000,
        autoplaySpeed: 1000,
        fade: true,
        cssEase: 'linear',
        nextArrow: '.slick-next',
        prevArrow: '.slick-prev',
        responsive: [
          {
            breakpoint: 500,
            settings: 'unslick',
          },
        ],
      });
    }

    knitwearSlickify();
    $(window).resize(function(){
        var $windowWidth = $(window).width();
        if ($windowWidth > 500) {
          knitwearSlickify();
        }
    });

     /* Recent post slider */
    if ($('.recent-posts').length) {
      new Glide('.recent-posts', {
        type: 'carousel',
        perView: 3,
        gap: 40,
        breakpoints: {
          576: {
            perView: 2,
          },
        },
      }).mount();
    }

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
