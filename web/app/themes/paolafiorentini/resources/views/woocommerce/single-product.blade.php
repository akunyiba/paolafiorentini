{{--
The Template for displaying all single products
This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.
@see 	    https://docs.woocommerce.com/document/template-structure/
@author 		WooThemes
@package 	WooCommerce/Templates
@version     1.6.4
--}}

@extends('layouts.app')

@section('content')
  @php
    do_action('get_header', 'shop');
    do_action('woocommerce_before_main_content');
    $product_id = get_the_ID();
    $product = wc_get_product( $product_id );
    $attributes = $product->get_attributes();
    $terms = wp_get_post_terms( $product_id, 'product_cat' );
    foreach ( $terms as $term ) $categories[] = $term->slug;
  @endphp

  @if( in_array( 'workshops', $categories ) )
    @include('partials.workshop-single-product')
  @elseif( in_array( 'knitwear', $categories ) )
    @include('partials.knitwear-single-product')
  @endif
@endsection
