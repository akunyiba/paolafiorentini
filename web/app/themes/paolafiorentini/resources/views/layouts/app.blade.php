<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <div class="w-full flex flex-col overflow-hidden min-h-screen md:overflow-visible">
      @php do_action('get_header') @endphp
      @include('partials.header')
      <div class="content flex flex-auto" role="document">
        <main class="main w-full h-auto flex flex-col md:pt-10" role="main">
          @if(is_cart() || is_checkout()) @php //TODO: @endphp
            <div class="alignwide">
              @yield('content')
            </div>
          @else
            @yield('content')
          @endif
        </main>
      </div>
      @php do_action('get_footer') @endphp
      @php //TODO: @endphp
      @if( !is_cart() && !is_checkout() )
        @include('partials.footer')
      @endif
      @php wp_footer() @endphp
    </div>
  </body>
</html>
