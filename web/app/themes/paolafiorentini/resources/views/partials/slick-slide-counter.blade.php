<div class="pf-slick-counter">
  <span class="pf-slick-counter__current"></span>
  <span class="text-2xl font-medium">/</span>
  <span class="pf-slick-counter__total"></span>
</div>
