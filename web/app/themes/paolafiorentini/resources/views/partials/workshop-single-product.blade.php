@php
$product_id = get_the_ID();

$related_args = array(
  'post_type' => 'product',
  'product_cat' => 'workshops',
  'posts_per_page' => 1,
  'post__not_in' => array($product_id)
);
$related_loop = new WP_Query( $related_args );

if ( $related_loop->have_posts() ) {
  $related_product = true;
  while ( $related_loop->have_posts() ) : $related_loop->the_post();
    $related_product_title = get_the_title();
    $related_product_url = get_the_permalink();
  endwhile;
} else {
  $related_product = false;
}
wp_reset_postdata();
@endphp

<div x-data="{color: '', size: ''}" id="product-{{ $product_id }}" {{ wc_product_class( 'h-full relative', $product_id ) }}>
  @include('partials.product-aside', ['link_url' => $related_product_url, 'link_text' => $related_product_title])
  <div class="h-full md:flex alignwide">
    <div class="md:w-1/2">
      @include('partials.product-slider')
    </div>
    <div class="md:w-1/2 entry-content relative">
      <div class="h-full w-full px-6 md:px-16 xl:max-w-3xl xl:ml-auto xl:mr-auto md:pb-64 pt-10 lg:pt-24">
        <h2 class="pf-product__title">{{ $product->get_title() }}</h2>
        @php wc_get_template_part('content', 'single-product'); @endphp
      </div>
    </div>
  </div>
</div>
