@php
  global $woocommerce;
  $cart_count = $woocommerce->cart->cart_contents_count;
@endphp
<header class="banner" x-data="{ open: false }" role="banner">
  <div class="px-4 lg:px-0">
    <nav class="relative flex flex-col items-center pb-8 lg:pb-0 justify-between h-30 lg:h-48 alignwide">
      <div class="absolute inset-y-0 left-0 flex items-center lg:hidden z-50">
        <button @click="open = !open; document.body.classList.toggle('overflow-hidden'); document.body.classList.toggle('h-screen'); document.body.classList.toggle('fixed');" class="hamburger hamburger--spring focus:outline-none p-0 flex" :class="{ 'is-active': open }" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
      </div>
      @if( function_exists('pll_the_languages') )
        <div class="hidden lg:flex absolute inset-y-0 lg:top-auto left-0 items-center pl-3 z-10">
          <ul class="lang-switcher pt-3 pl-0 list-none flex">
            @php pll_the_languages( array('dropdown'=>1) ); @endphp
          </ul>
        </div>
      @endif
      <a class="brand mt-8 lg:mt-16 hover:border-transparent" href="{{ function_exists('pll_home_url') ? pll_home_url() : home_url() }}">
        <img class="h-16 w-auto" src="@asset('images/logo.svg')" alt="{{ get_bloginfo('name', 'display') }}" />
      </a>
      <div class="hidden lg:block relative w-full">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav flex justify-center']) !!}
        @endif
      </div>
      <div class="absolute inset-y-0 lg:top-auto right-0 flex items-center pr-3">
        <button class="relative p-1 border-2 border-transparent rounded-none focus:outline-none transition duration-150 ease-in-out">
          <a href="{{ wc_get_cart_url() }}" class="text-black hover:text-gray-100">
            <svg class="w-8 h-8 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.54 86.45"><g data-name="Layer 2"><g data-name="Layer 1"><path d="M72.47 24.35v59H3.07v-59h69.4m3.07-3.07H0v65.17h75.54V21.28z" fill="currentColor"/><path d="M16.08 21.24C27.13-5 48.68-5 59.73 21.24" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="3.07"/></g></g></svg>
            @if( $cart_count > 0 )
              <div class="cart-count leading-none flex justify-center items-center bg-primary w-6 h-6 rounded-full absolute top-0 font-din font-bold text-black" style="right: -10px;"><span class="h-full">{{ $woocommerce->cart->cart_contents_count }}</span></div>
            @endif
          </a>
        </button>
      </div>
    </nav>
  </div>
  <div x-show.transition.in.duration.500ms.origin.center.left.out.duration.500ms.origin.center.right="open" :class="{'hidden': !open }" class="hidden z-40 fixed left-0 top-0 w-screen h-screen bg-primary">
    <div class="pt-10 pl-20 pr-6">
      <div class="relative" style="margin-top: -1px;">
        @if (has_nav_menu('mobile_navigation'))
        {!! wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'nav-mobile flex p-0 list-none']) !!}
        @endif
      </div>
    </div>
  </div>
</header>
