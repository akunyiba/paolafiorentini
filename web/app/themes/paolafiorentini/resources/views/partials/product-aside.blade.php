@php 
  if(!isset($link_url)) $link_url = '/';
  if(!isset($link_text)) $link_text = __('Home', 'sage');
@endphp

<aside x-data="{hover: false}" class="pf-product-aside">
  <a @mouseover="hover = !hover" @mouseout="hover = !hover" href="{{ $link_url }}">
    <svg class="transform -rotate-90" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg>
    <span class="pf-product-aside__link-text" style="writing-mode: vertical-rl;">{{ $link_text }}</span>
  </a>
</aside>