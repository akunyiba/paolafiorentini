@php
  global $product;
  $attachment_ids = $product->get_gallery_image_ids();
@endphp

<div class="pf-glide pf-glide--vertical">
  <div class="glide h-full">
    <div class="glide__track h-full" data-glide-el="track">
      <ul class="glide__slides h-full">
        @foreach( $attachment_ids as $attachment_id )
          @php $gallery_image_url = wp_get_attachment_url( $attachment_id ); @endphp
          <div class="glide__slide" style="background-image: url({{ $gallery_image_url }});"></div>
        @endforeach
      </ul>
    </div>
    <div class="glide__arrows hidden md:block" data-glide-el="controls">
      <button x-data="{hover: false}" class="glide__arrow glide__arrow--left transform-none" data-glide-dir="{{ '<' }}"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" d="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
      <button x-data="{hover: false}" class="glide__arrow glide__arrow--right transform-none" data-glide-dir="{{ '>' }}"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
    </div>
  </div>
  <div class="hidden md:flex justify-center items-center border-none bg-white border-l border-gray-100 text-gray-100 w-20 h-20 absolute left-0 bottom-0">
    @include('partials.glide-slide-counter')
  </div>
</div>
