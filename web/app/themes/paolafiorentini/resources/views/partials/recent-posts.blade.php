@php
  isset( $type ) ? '' : $type = 'post';
  isset( $product_cat ) ? '' : $product_cat = '';
  $args = array(
    'post_type' => $type,
    'posts_per_page' => 9,
    'product_cat' => $product_cat,
    'no_found_rows' => true,
    'post__not_in' => array( get_the_ID() )
  );
  $post_query = new WP_Query($args);
  $posts = $post_query->get_posts();
@endphp

<section class="recent-posts glide">
  <div class="glide__track" data-glide-el="track">
    <ul class="glide__slides">
      @foreach ($posts as $post)
        @php
          $title = $post->post_title;
          $read_more_link = get_the_permalink($post->ID);
          $image_url = get_the_post_thumbnail_url( $post->ID, 'full' );
          if ( !$image_alt = get_the_post_thumbnail_caption( $post->ID ) ) {
            $image_alt = get_the_title( $post->ID );
          }
        @endphp
        <div class="recent-post glide__slide">
          <a href="{{ esc_url($read_more_link) }}" class="text-black border-none">
            <div class="img-hover-zoom img-hover-zoom--colorize h-48 md:h-64">
              <img src="{{ esc_url($image_url) }}" alt="{{ esc_attr($image_alt) }}" class="w-full object-cover" style="height: 100%;">
            </div>
            <h4 class="mt-2 pl-6">{{ $title }}</h4>
          </a>
        </div>
      @endforeach
    </ul>
  </div>
  <div class="glide__arrows" data-glide-el="controls">
    <button x-data="{hover: false}" class="glide__arrow glide__arrow--left transform -rotate-90 left-0" data-glide-dir="{{ '<' }}"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" d="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
    <button x-data="{hover: false}" class="glide__arrow glide__arrow--right transform rotate-90 right-0" data-glide-dir="{{ '>' }}"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
  </div>
</section>
