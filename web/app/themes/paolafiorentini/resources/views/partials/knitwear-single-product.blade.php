@php
  $shop_page_link_url = get_permalink( 233 ); //TODO: static reference
  $shop_page_link_text = __('torna a collezione', 'sage');
@endphp

<div x-data="{color: '', size: ''}" id="product-{{ $product_id }}" {{ wc_product_class( 'h-full relative', $product_id ) }}>
  @include('partials.product-aside', ['link_url' => $shop_page_link_url, 'link_text' => $shop_page_link_text])
  <div class="h-full md:flex alignwide">
    <div class="md:w-1/2">
      @include('partials.product-slider')
    </div>
    <div class="md:w-1/2 entry-content relative">
      <div class="h-full w-full px-6 md:px-16 xl:max-w-3xl xl:ml-auto xl:mr-auto pt-12 lg:pb-64 lg:pt-24">
        <h2 class="font-axis md:hidden">{{ $product->get_title() }}</h2>
        <div class="hidden md:block">
          <h2 class="font-axis">{{ $product->get_title() }}</h2>
          <p class="font-bold">{{ get_the_excerpt( $product_id ) }}</p>
        </div>
        @php wc_get_template_part('content', 'single-product'); @endphp
        <div class="lg:absolute hidden md:block" style="bottom: 3rem;">
          <p>{{ get_field('product-footer') }}</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="md:hidden alignfull mt-10 mb-24">
  @include('partials.recent-posts', array('type' => 'product', 'product_cat' => 'knitwear'))
</div>
