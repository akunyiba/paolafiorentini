{{--
  Title: Hero Slider
  Description: Hero Slider
  Category: pf-blocks
  Icon: slides
  Keywords: hero, slider
  Mode: preview
  Align: wide
  PostTypes: page post
  SupportsAlign: false
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'pf-hero-slider-' . $block['id'];
  if( !empty($block['anchor']) ) :
    $id = $block['anchor'];
  endif;

  $className = 'pf-hero-slider pf-glide h-full relative flex-1';

  if( !empty($block['className']) ) :
    $className .= ' ' . $block['className'];
  endif;

  if( !empty($block['align']) ) :
    $className .= ' align' . $block['align'];
  endif;

  // Load values and assing defaults.
  $rows = get_field('hero_slider');
@endphp

@if ($rows)
  <section id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
    <div class="glide h-full">
      <div class="glide__track h-full" data-glide-el="track">
        <ul class="glide__slides h-full">
          @foreach ($rows as $item)
            @php
              $title = $item['title'];
              $desc = $item['description'];
              $image_url = $item['image'];
              $price = $item['price'];
              $btn_text = $item['button']['button_text'];
              $btn_link = $item['button']['button_link'];
            @endphp
            <div class="glide__slide h-full">
              <div class="h-full p-4 bg-cover bg-bottom" style="background-image: url({{ $image_url }});">
                <div class="h-full lg:flex justify-center items-center lg:items-end">
                  <div class="2xl:mb-16 px-6 lg:w-1/2 lg:ml-auto lg:px-0">
                    <h1 class="text-primary mt-10">{{ $title }}</h1>
                    <p class="text-black"><strong>{{ $desc }}</strong></p>
                    <div class="flex mt-10">
                      <div class="btn btn--big btn--black px-3 md:px-5 hover:bg-black hover:text-white flex items-center">€{{ $price }}</div>
                      <button class="btn btn--big px-6 sm:px-8 md:px-16">
                        <a href="{{ $btn_link }}">{{ $btn_text }}</a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
        </ul>
      </div>
      <div class="glide__arrows hidden lg:block" data-glide-el="controls">
        <button x-data="{hover: false}" class="glide__arrow glide__arrow--left transform-none" data-glide-dir="{{ '<' }}"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" d="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
        <button x-data="{hover: false}" class="glide__arrow glide__arrow--right transform-none" data-glide-dir="{{ '>' }}"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
      </div>
    </div>
    <div class="hidden lg:flex justify-center items-center border-none bg-white border-l border-gray-100 text-gray-100 w-20 h-20 absolute left-0 bottom-0">
      @include('partials.glide-slide-counter')
    </div>
  </section>
@endif
