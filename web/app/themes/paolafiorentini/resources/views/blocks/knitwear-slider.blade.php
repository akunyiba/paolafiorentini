{{--
  Title: Knitwear Slider
  Description: Knitwear Slider
  Category: pf-blocks
  Icon: slides
  Keywords: knitwear, slider
  Mode: preview
  PostTypes: page post
  Align: wide
  SupportsAlign: false
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'pf-knitwear-slider-' . $block['id'];
  if( !empty($block['anchor']) ) :
    $id = $block['anchor'];
  endif;

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'pf-knitwear-slider h-full relative flex flex-wrap';

  if( !empty($block['className']) ) :
    $className .= ' ' . $block['className'];
  endif;

  if( !empty($block['align']) ) :
    $className .= ' align' . $block['align'];
  endif;

  // Load values and assing defaults.
  $args = array(
          'post_type' => 'product',
          'posts_per_page' => -1,
          'product_cat' => 'knitwear',
          'orderby' => 'menu_order',
          'order' => 'ASC',
      );
  $query = new WP_Query($args);
  $products = $query->get_posts();
@endphp

<section id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
  <button x-data="{hover: false}" class="slick-prev"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" d="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
  <div class="pf-slick w-full h-full flex flex-col">
    @foreach ($products as $products)
      @php
        $title = $products->post_title;
        $excerpt = $products->post_excerpt;
        $more_link = get_the_permalink($products->ID);
        $more_text = __('Scopri', 'sage');

        $image_id = get_post_thumbnail_id($products->ID);
        $img_src = wp_get_attachment_image_url( $image_id, 'large' );
        $img_srcset = wp_get_attachment_image_srcset( $image_id, 'full' );
        $img_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
      @endphp
      <a class="pf-slick__item border-none" x-data="{ hover: false }" href="{{ esc_url( $more_link ) }}">
        <div
        @mouseover="hover = !hover"
        @mouseout="hover = !hover"
        class="h-full relative flex justify-center items-center"
        style="min-height: 200px;">
          <div class="flex flex-col justify-center items-center h-full">
            <div class="px-6 py-6 lg:py-0 lg:px-10">
              <h3 class="text-2xl md:text-5xl font-axis transition ease-in duration-300 text-black">{{ $title  }}</h3>
              <p class="text-sm md:text-lg text-black font-acumin font-bold">{{ $excerpt }}</p>
            </div>
            <div class="absolute w-full h-full z-negative top-0">
              <img class="object-cover w-full h-full" src="{{ esc_attr( $img_src ) }}" srcset="{{ esc_attr( $img_srcset ) }}" sizes="100vw" alt="{{ $img_alt }}">
            </div>
          </div>
          <div :class="{ 'opacity-75': hover, 'opacity-0': !hover}"" class="absolute w-full h-full bg-primary top-0 left-0 z-negative transition ease-in duration-300"></div>
        </div>
      </a>
    @endforeach
  </div>
  <button x-data="{hover: false}" class="slick-next"><svg @mouseover="hover = !hover" @mouseout="hover = !hover" d="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 117"><title>arrow_icon</title><rect :class="{'text-primary fill-current': hover}" class="transition ease-in duration-300" width="117" height="117"/><path :class="{'text-black': hover, 'text-white': !hover}" class="fill-current transition ease-in duration-500" d="M57.19,36.51a.74.74,0,0,0-.54.23L45.07,48.32a.76.76,0,0,0-.24.56.8.8,0,0,0,.24.57.78.78,0,0,0,.55.22.8.8,0,0,0,.55-.22l9.36-9.36.87-.88V71.33a.78.78,0,0,0,.78.79.79.79,0,0,0,.8-.79V39.21l.88.88,9.34,9.36a.83.83,0,0,0,1.12,0,.8.8,0,0,0,.24-.57.75.75,0,0,0-.23-.55L57.75,36.75A.76.76,0,0,0,57.19,36.51Z" transform="translate(0.55 0.5)"/></svg></button>
  @include('partials.slick-slide-counter')
</section>
