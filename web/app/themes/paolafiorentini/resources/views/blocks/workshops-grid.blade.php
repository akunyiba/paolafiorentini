{{--
  Title: Workshops Grid
  Description: Workshops Grid
  Category: pf-blocks
  Icon: slides
  Keywords: workshop, woskhops, grid
  Mode: preview
  PostTypes: page post
  Align: full
  SupportsAlign: false
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'pf-workshops-' . $block['id'];
  if( !empty($block['anchor']) ) :
    $id = $block['anchor'];
  endif;

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'pf-workshops alignwide h-full relative flex flex-col lg:flex-row py-20 lg:py-0';

  if( !empty($block['className']) ) :
    $className .= ' ' . $block['className'];
  endif;

  if( !empty($block['align']) ) :
    $className .= ' align' . $block['align'];
  endif;

  // Load values and assing defaults.
  $args = array(
          'post_type' => 'product',
          'posts_per_page' => 2,
          'product_cat' => 'workshops',
          'order' => 'ASC'
      );
  $query = new WP_Query($args);
  $products = $query->get_posts();
@endphp

<section id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
  @foreach ($products as $products)
    @php
      $title = $products->post_title;
      $more_link = get_the_permalink($products->ID);
      $more_text = __('Scopri', 'sage');

      $image_id = get_post_thumbnail_id($products->ID);
      $img_src = wp_get_attachment_image_url( $image_id, 'large' );
      $img_srcset = wp_get_attachment_image_srcset( $image_id, 'full' );
      $img_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
    @endphp
    <a class="pf-workshop-products__product h-full lg:w-1/2 border-none" x-data="{ hover: false }" href="{{ esc_url( $more_link ) }}">
      <div
      @mouseover="hover = !hover"
      @mouseout="hover = !hover"
      class="h-full relative"
      >
        <div class="p-6 lg:p-0 flex flex-col justify-center items-center h-full">
          <h3 class="pf-workshop-products__product-mobile-title">{{ $title  }} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 94.13 65.38"><title>arrow-right</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M94.13,32.69a2,2,0,0,0-.61-1.44L62.9.63A2,2,0,0,0,61.43,0a2.1,2.1,0,0,0-1.5.63,2,2,0,0,0-.59,1.46,2,2,0,0,0,.59,1.45L84.68,28.29,87,30.6H2.06A2,2,0,0,0,0,32.66a2.09,2.09,0,0,0,2.06,2.11H87l-2.32,2.31L59.93,61.79a2.14,2.14,0,0,0,0,3,2.09,2.09,0,0,0,1.5.63,2,2,0,0,0,1.45-.61L93.5,34.15A2,2,0,0,0,94.13,32.69Z"></path></g></g></svg></h3>
          <h3 class="font-axis hidden lg:block text-center text-4xl uppercase transition ease-in duration-300" :class="{ 'text-white': hover, 'text-gray-100': !hover }">{{ $title  }}</h3>
          <div class="hidden lg:block absolute w-full h-full z-negative top-0">
            <img class="object-cover w-full h-full" src="{{ esc_attr( $img_src ) }}" srcset="{{ esc_attr( $img_srcset ) }}" sizes="100vw" alt="{{ $img_alt }}">
          </div>
          <button class="hidden lg:block mt-24 btn transition ease-in duration-300" :class="{ 'btn--black': !hover }">{{ __('Scopri', 'sage') }}</button>
        </div>
        <div :class="{ 'opacity-50': hover, 'opacity-0': !hover}" class="absolute w-full h-full bg-black top-0 left-0 z-negative transition ease-in duration-300"></div>
      </div>
    </a>
  @endforeach
</section>
