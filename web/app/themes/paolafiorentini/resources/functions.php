<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);




/**
 *  WooCommerce change add to cart text on single product page
 * @link https://www.codegearthemes.com/blogs/woocommerce/woocommerce-how-to-change-add-to-cart-button-text
 */
add_filter('woocommerce_product_single_add_to_cart_text', function () {
    $terms = wp_get_post_terms(get_the_ID(), 'product_cat');
    $btn_text = '';
    foreach ($terms as $term) $categories[] = $term->slug;

    if (in_array('workshops', $categories)) {
        $btn_text = __('Prenota', 'sage');
    } elseif (in_array('knitwear', $categories)) {
        $btn_text = __('Aggiungi', 'sage');
    } else {
        $btn_text = __('Add to cart', 'sage');
    }
    return $btn_text;
});

/**
 *  WooCommerce change add to cart text on product archives (Collection) page
 * @link https://www.codegearthemes.com/blogs/woocommerce/woocommerce-how-to-change-add-to-cart-button-text
 */
add_filter('woocommerce_product_add_to_cart_text', function () {
    return __('Scopri', 'sage');
});

/**
 *  WooCommerce customize form fields
 * @link https://stackoverflow.com/questions/46326620/custom-placeholder-for-all-woocommerce-checkout-fields
 */
add_filter( 'woocommerce_checkout_fields' , function ( $fields ) {
    $fields['billing']['billing_first_name']['placeholder'] = __('First name', 'sage');
    $fields['billing']['billing_last_name']['placeholder'] = __('Last name', 'sage');
    $fields['billing']['billing_company']['placeholder'] = __('Company name', 'sage');
    $fields['billing']['billing_postcode']['placeholder'] = __('Postcode / ZIP', 'sage');
    $fields['billing']['billing_city']['placeholder'] = __('Town / City', 'sage');
    $fields['billing']['billing_phone']['placeholder'] = __('Phone', 'sage');
    $fields['billing']['billing_email']['placeholder'] = __('Email address', 'sage');

    $fields['shipping']['shipping_first_name']['placeholder'] = $fields['billing']['billing_first_name']['placeholder'];
    $fields['shipping']['shipping_last_name']['placeholder'] = $fields['billing']['billing_last_name']['placeholder'];
    $fields['shipping']['shipping_company']['placeholder'] = $fields['billing']['billing_company']['placeholder'];
    $fields['shipping']['shipping_postcode']['placeholder'] = $fields['billing']['billing_postcode']['placeholder'];
    $fields['shipping']['shipping_city']['placeholder'] = $fields['billing']['billing_city']['placeholder'];
    $fields['shipping']['shipping_phone']['placeholder'] = $fields['billing']['billing_phone']['placeholder'];
    $fields['shipping']['shipping_email']['placeholder'] = $fields['billing']['billing_email']['placeholder'];
    return $fields;
}, 20, 1 );
